<?php


namespace App\Service;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use phpseclib\Net\SFTP;

class Alpha extends AbstractController
{
    private $connection;
    private $sftp;
    private $directoryBase = "/mnt/data/VMS/MGELLOGEMENT/";

    public function __construct( )
    {
    }

    public function connectAlpha()
    {
        $this->connection = ssh2_connect($_ENV['SERVER_ALPHA_IP']);
        if (!$this->connection) return(false);
        ssh2_auth_password($this->connection, $_ENV['SERVER_ALPHA_USERNAME'], $_ENV['SERVER_ALPHA_PASSWORD']);
        $this->sftp = ssh2_sftp($this->connection);
    }

    public function getNewFileAlpha($directory){
        $this->connectAlpha();
        $listFiles = [];
        $handle2 = opendir('ssh2.sftp://' . intval($this->sftp) . $this->directoryBase . $directory);
        while (false !== ($file = readdir($handle2))) {
            if ($file != "." && $file != "..") {
                if(is_file ('ssh2.sftp://' . intval($this->sftp) . $this->directoryBase . $directory. $file))
                    $listFiles[] = $file;
            }
        }
        if(count($listFiles) == 0){
            return false;
        }
        asort($listFiles);
        return array_shift($listFiles);
    }

    public function getFileAlpha($directory, $file){
        $this->connectAlpha();

        return fopen('ssh2.sftp://' . intval($this->sftp) . $this->directoryBase . $directory.$file , 'r');
    }

    public function uploadFiles($directory, $files){
        $this->connectAlpha();
        foreach ($files as $file){
            foreach ($file as $fileName => $content){
                $handle = fopen('ssh2.sftp://' . intval($this->sftp) . $this->directoryBase . $directory.$fileName , 'w');
                fwrite($handle, $content);
                fclose($handle);
            }
        }
        return(true);
    }

    public function archiveFileAlpha($directory, $file){
        $this->connectAlpha();

        ssh2_sftp_rename ($this->sftp , $this->directoryBase . $directory.$file , $this->directoryBase . $directory.'OLD/'.$file );
    }

    public function makeDirectory($directory)
    {
        $this->connectAlpha();

        return ssh2_sftp_mkdir ($this->sftp , $this->directoryBase . $directory);
    }


}