<?php

namespace App\Service;

use App\DTO\ClientDto;
use App\Entity\Prospect;
use CURLFile;
use http\Env\Request;
use Psr\Log\LoggerInterface;
use App\Service\HttpClientHelper;
use App\Traits\CurlFileGeneratorTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Signature
{
    use CurlFileGeneratorTrait;
    public function __construct(private HttpClientHelper $httpClient, private SessionInterface $session)
    {

    }
    public  function getDocumentSigned($signatureId){

        if(!$signatureId){
            return $signatureId;
        }

        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "j_token: ".$_ENV['SIGN_TOKEN']
            ]
        ];

        // DOCS: https://www.php.net/manual/en/function.stream-context-create.php
        $context = stream_context_create($opts);

        // Open the file using the HTTP headers set above
        // DOCS: https://www.php.net/manual/en/function.file-get-contents.php
        try {
            $file = file_get_contents($_ENV['SIGN_HOST_V4'].'contracts/'.$signatureId.'/transaction/signedcontract?filename=contract.pdf', false, $context);
        }catch (\Exception $e){
            return false;
        }

        if($file){
            return $file;
        }

        return false;
    }

    public function signSimple($data, $document ) {

        $fileNameTmp = time().'.pdf';
        file_put_contents($fileNameTmp, $document);
        $curlFile = new CURLFile($fileNameTmp);
        $curlFile->setMimeType('application/pdf');
        $curlFile->setPostFilename("devis.pdf");

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $_ENV['SIGN_HOST_V4'].'/contracts/allinone?start=false',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('contract' => '{
            "name": "'.$data->civilite1->value.' '.$data->nom_prenom_du_client->value.' '.$data->prenom_du_client->value.'.pdf",
            "contract_definition_id": '.$_ENV['SIGN_CONTRACT_DEFINITION_ID'].',
            "vendor_email": "'.$_ENV['SIGN_VENDOR_EMAIL'].'",
            "message_title": "Votre contrat EUROPE 13 pour signature",
            "message_body": "Vous êtes signataire du devis ci-joint pour Europenvironnement. Merci de bien vouloir le signer électroniquement en cliquant sur le lien ci-dessous.<br>Cordialement",
            "keep_on_move": false,
            "perimeters": ["'.$_ENV['SIGN_PERIMETER'].'"],
            "auto_close": 1
            }','recipients' => '{
            "data": [
                    {
                        "civility": "'.$data->civilite1->value.'",
                        "firstname": "'.$data->nom_prenom_du_client->value.'",
                        "lastname": "'.$data->prenom_du_client->value.'",
                        "address_1": "'.$data->adresse1->value.'",
                        "address_2": "'.$data->adresse2_address->value.'",
                        "postal_code": "'.$data->adresse2_zip->value.'",
                        "city": "'.$data->adresse2_city->value.'",
                        "cell_phone": "'.$data->telephone->value.'",
                        "email": "'.$data->mail->value.'",
                        "signature_mode": 1
                    }
                ]
            }','pdfparts'=> $curlFile),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: multipart/form-data',
                'j_token: '.$_ENV['SIGN_TOKEN']
            ),
        ));

        // "pvid_identity_level": 1, : id verify

        $response = curl_exec($curl);

        if(!$result = json_decode($response, 1)){
            return false;
        }


        curl_close($curl);


        $dateExpiration = strtotime('+2 days', time())*1000;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $_ENV['SIGN_HOST_V4'].'/contracts/'.$result['contract']['contract_id'].'/transaction/temporarytoken?signonly=true&isbundle=false',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
            "actor_id": '.$_ENV['SIGN_ACTOR_ID'].',
            "contract_definition_id": '.$_ENV['SIGN_CONTRACT_DEFINITION_ID'].',
            "recipient_id": '.$result['recipientForcontract'][0]['recipient_id'].',
            "validity_duration": '.$dateExpiration.'
        }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'j_token: '.$_ENV['SIGN_TOKEN']
            ),
        ));

        $response = curl_exec($curl);

        $token = json_decode($response, 1);

        curl_close($curl);


        $tempToken = urlencode($token['token']);

        // final link
        $signatureUrl = $_ENV['SIGN_HOST']."s/generic_sign_contract_index.html?l_id=".$_ENV['SIGN_LICENCE_ID']."&direct_contract=".$_ENV['SIGN_CONTRACT_DEFINITION_ID']."&cd_id=".$_ENV['SIGN_CONTRACT_DEFINITION_ID']."&c_id=".$result['recipientForcontract'][0]['contract_id']."&page=1&customer_number=".$result['recipientForcontract'][0]['recipient_id']."&j_token=".$tempToken;

        if(is_file($fileNameTmp)){
            unlink($fileNameTmp);
        }

        return [ 'idContract' => $result['contract']['contract_id'], 'link' => $signatureUrl];


    }

    public function signAdvanced($data, $document ) {

        $fileNameTmp = time().'.pdf';
        file_put_contents($fileNameTmp, $document);
        $curlFile = new CURLFile($fileNameTmp);
        $curlFile->setMimeType('application/pdf');
        $curlFile->setPostFilename("devis.pdf");

        $curl = curl_init();

        $body = array(
            CURLOPT_URL => $_ENV['SIGN_HOST_V4'].'/contracts/allinone?start=true',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('contract' => '{
            "name": "'.$data->civilite1->value.' '.$data->nom_prenom_du_client->value.' '.$data->prenom_du_client->value.'.pdf",
            "contract_definition_id": '.$_ENV['SIGN_CONTRACT_DEFINITION_ID'].',
            "vendor_email": "'.$_ENV['SIGN_VENDOR_EMAIL'].'",
            "message_title": "Votre contrat EUROPE 13 pour signature",
            "message_body": "Vous êtes signataire du devis ci-joint pour Europenvironnement. Merci de bien vouloir le signer électroniquement en cliquant sur le lien ci-dessous.<br>Cordialement",
            "keep_on_move": false,
            "perimeters": ["'.$_ENV['SIGN_PERIMETER'].'"],
            "auto_close": 1
            }','recipients' => '{
            "data": [
                    {
                     "civility": "'.$data->civilite1->value.'",
                        "firstname": "'.$data->prenom_du_client->value.'",
                        "lastname": "'.$data->nom_prenom_du_client->value.'",
                        "address_1": "'.$data->adresse1->value.'",
                        "address_2": "'.$data->adresse2_address->value.'",
                        "postal_code": "'.$data->adresse2_zip->value.'",
                        "city": "'.$data->adresse2_city->value.'",
                        "cell_phone": "'.$data->telephone->value.'",
                        "email": "'.$data->mail->value.'",
                    "signature_mode": 19,
                    "pvid_identity_level": 2,
                    "birthdate" : "'.(strtotime($data->birthday)*1000).'",
                    "birthplace": "'.$data->birthplace.'",
                    "country": "FRANCE"
                    }
                ]
            }','pdfparts'=> $curlFile),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: multipart/form-data',
                'j_token: '.$_ENV['SIGN_TOKEN']
            ),
        );

        curl_setopt_array($curl, $body);

        // "pvid_identity_level": 1, : id verify

        $response = curl_exec($curl);

        if(!$result = json_decode($response, 1)){
            return false;
        }


        curl_close($curl);



        $dateExpiration = strtotime('+2 days', time())*1000;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $_ENV['SIGN_HOST_V4'].'/contracts/'.$result['contract']['contract_id'].'/transaction/temporarytoken?signonly=true&isbundle=false',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
            "actor_id": '.$_ENV['SIGN_ACTOR_ID'].',
            "contract_definition_id": '.$_ENV['SIGN_CONTRACT_DEFINITION_ID'].',
            "recipient_id": '.$result['recipientForcontract'][0]['recipient_id'].',
            "validity_duration": '.$dateExpiration.'
        }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'j_token: '.$_ENV['SIGN_TOKEN']
            ),
        ));

        $response = curl_exec($curl);

        $token = json_decode($response, 1);

        curl_close($curl);


        $tempToken = urlencode($token['token']);

        // final link
        $signatureUrl = $_ENV['SIGN_HOST']."s/generic_sign_contract_index.html?l_id=".$_ENV['SIGN_LICENCE_ID']."&direct_contract=".$_ENV['SIGN_CONTRACT_DEFINITION_ID']."&cd_id=".$_ENV['SIGN_CONTRACT_DEFINITION_ID']."&c_id=".$result['recipientForcontract'][0]['contract_id']."&page=1&customer_number=".$result['recipientForcontract'][0]['recipient_id']."&j_token=".$tempToken;


        if(is_file($fileNameTmp)){
            unlink($fileNameTmp);
        }

        return [ 'idContract' => $result['contract']['contract_id'], 'link' => $signatureUrl];


    }

    public function signatureStatus($signatureId) {

        $result =  $this->httpClient->httpCall(
            method : "GET",
            url : $_ENV['SIGN_HOST_V4'].'contracts/'.$signatureId.'/transaction/recipients',
                params :[
            'headers' =>[
                'Content-Type: application/json',
                'j_token: '.$_ENV['SIGN_TOKEN']
            ]
        ]);

        if(!isset($result['isSuccess']) || (isset($result['isSuccess']) && !$result['isSuccess'])){
            return false;
        }
        return $result['response'][0];

    }

}
