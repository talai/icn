let examen = {
            qst : {},
            begin : true,
            currentId : null,
            currentQstStruct : {q2 : false, c : false, d : false },
            cleanChoice : function(choice){
                if(choice[0] == '!') return choice.substr(1);
                else return choice;
            },
            saveResponse : function(){
                that = this;
                examen.qst[this.currentId].userReponses = [];
                $.each($('#blocQst input:checkbox:checked'), function (i, item) {
                    examen.qst[that.currentId].userReponses.push(item.value)
                });

                this.correction()


            },
            correction : function(){

                qst = this.qst[examen.currentQstGroup][this.currentId]
                var responsesC = [];
                if(qst[4] != "" && qst[4][0] == '!')
                    responsesC.push('A');
                
                if(qst[5] != "" && qst[5][0] == '!')
                    responsesC.push('B');

                if(qst[7] != "" && qst[7][0] == '!')
                    responsesC.push('C');
                
                if(qst[8] != "" && qst[8][0] == '!')
                    responsesC.push('D');

                this.qst[examen.currentQstGroup][this.currentId].isCorrect = responsesC.equals(this.qst[examen.currentQstGroup][this.currentId].userReponses)

            },

            array_diff : function(a1, a2) {

                var a = [], diff = [];

                for (var i = 0; i < a1.length; i++) {
                    a[a1[i]] = true;
                }

                for (var i = 0; i < a2.length; i++) {
                    if (a[a2[i]]) {
                        delete a[a2[i]];
                    } else {
                        a[a2[i]] = true;
                    }
                }

                for (var k in a) {
                    diff.push(k);
                }

                return diff;
            },

            end : function(){

                datas = {};
                examen.fautes = 0;
                for(i in examen.qst){

                    if(!examen.qst[i].isCorrect)
                        examen.fautes++;
                    
                    datas[examen.qst[i][0]] = {
                        'userReponses' : examen.qst[i].userReponses,
                        'compare' : examen.qst[i].isCorrect,
                    }; 


                }
                
                $.ajax({
                    type: "POST",
                    url: 'controller.php',
                    data: {action : 'saveExam', exam : datas},
                    dataType: 'JSON',
                    success: function(result){
                        if(result.status != 200){
                           window.location.replace("https://www.codeclic.com");
                        }else{
                            window.location.replace("resume.php");
                        }
                        
                    }
                });
                
                
            },
            play : function(){

                if(this.begin == true){
                    this.currentId = 0;
                    this.begin = false;

                }else{
                    
                    this.currentId++;
                }

                // stop
                if(this.currentId > 39){
                    this.end();
                    return;
                }
                    
                

                //reset
                $('#blocQst input:checkbox').prop('checked', false);;
                $.each($('audio'), function () {
                    $(this).stop();
                });
                $("#media").empty();

                  
                var qst = this.qst[examen.currentQstGroup][this.currentId];

                //image Or Video
                checkVideo = qst[1] in this.videos;
                if(checkVideo){
                    $("#imageQst").addClass('d-none');
                    var video = document.createElement('video');
                    var media = document.getElementById('media');
                    media.appendChild(video);
                    video.setAttribute('src', 'https://player.vimeo.com/external/'+this.videos[qst[1]]);
                    video.setAttribute('autoplay', 'autoplay');
                    video.setAttribute('playsinline', '');
                    video.setAttribute('controls', '');
                    video.setAttribute('muted', 'muted');
                    //video.setAttribute('loop', '');
                    video.setAttribute('oncanplay', "this.muted=true");
                    video.setAttribute('poster', 'https://www.codeclic.com/espace/video.jpg');
                    $(video).addClass('w-100');
                    
                }
                else{
                    $("#imageQst").attr('src', 'https://www.codeclic.com/photographies/'+qst[10])
                    $("#imageQst").removeClass('d-none');
                }
                    
                

                //Audio
                var audio = document.createElement('audio');
                audio.id = 'audio-player';
                var source = document.createElement('source');
                media = document.getElementById('media');
                media.appendChild(audio);
                audio.appendChild(source);
                source.setAttribute('src', 'https://www.codeclic.com/sons/'+qst[11]);
                source.setAttribute('type', 'audio/mpeg');
                audio.setAttribute('controls', 'controls');
                audio.setAttribute('autoplay', 'autoplay');
                $(audio).hide();
                


                $("#idQst").text(this.currentId + 1);
                
                //question A
                $("#qstA p").text(qst[3]);
                
                // Choix A,B
                var choiceA, choiceB, choiceC, choiceD = null;
                
                choiceA = this.cleanChoice(qst[4]);
                choiceB = this.cleanChoice(qst[5]);

                
                $("#blocChoiceA label").text(choiceA);
                $("#blocChoiceB label").text(choiceB);

                if(this.qst[examen.currentQstGroup][this.currentId][6]){
                    //question B
                    $("#qstB p").text(qst[6]);
                    this.currentQstStruct.q2 = true;
                    $("#qstB").show();
                }else{
                    $("#qstB p").text('');
                    this.currentQstStruct.q2 = false;
                    $("#qstB").hide();
                }

                //Choix C
                if(this.qst[examen.currentQstGroup][this.currentId][7]){
                    choiceC = this.cleanChoice(qst[7]);
                    $("#blocChoiceC label").text(choiceC);
                    this.currentQstStruct.c = true;
                    $("#blocChoiceC").show();
                }else{
                    $("#blocChoiceC label").text('');
                    this.currentQstStruct.c = false;
                    $("#blocChoiceC").hide();
                }

                //Choix D
                if(this.qst[examen.currentQstGroup][this.currentId][8]){
                    choiceD = this.cleanChoice(qst[8]);
                    $("#blocChoiceD label").text(choiceD);
                    this.currentQstStruct.d = true;
                    $("#blocChoiceD").show();
                }else{
                    $("#blocChoiceD label").text('');
                    this.currentQstStruct.d = false;
                    $("#blocChoiceD").hide();
                }
                
                this.qst[examen.currentQstGroup][this.currentId].struct = this.currentQstStruct;

            },
            
            videos : {},
              
        };