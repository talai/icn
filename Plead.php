<?php


namespace App\Service;


use RestClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Cache\Adapter\RedisTagAwareAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;

class Plead extends AbstractController
{
    public function getToken(){


        $authUrl = $_ENV['PLEAD_API_URL']."connexion/oauth2/access_token";
        $headers = array();
        $headers['Content-Type'] = "application/x-www-form-urlencoded";

        $data = [
            "grant_type" => "client_credentials",
            "client_id" => $_ENV['PLEAD_API_USER'],
            "client_secret" => $_ENV['PLEAD_API_PASSWORD']
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $authUrl);
        curl_setopt($ch, CURLOPT_POST, 1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        $auth = curl_exec( $ch );

        if ( curl_errno( $ch ) ){
            return false;
        }
        curl_close($ch);

        $secret = json_decode($auth);
        if(!isset($secret->access_token)){
            return false;
        }

        $cache = new RedisTagAwareAdapter(RedisAdapter::createConnection('redis://localhost'));
        $redisCache = $cache->getItem('plead_token');
        $redisCache->set($secret->access_token);
        $cache->save($redisCache);
        return $secret->access_token;
    }

    public function request($url, $isPost = true, $data = [], $isDelete = false){

        $cache = new RedisTagAwareAdapter(RedisAdapter::createConnection('redis://localhost'));
        $redisCache = $cache->getItem('plead_token');
        if(!$redisCache->isHit()){
            $token = $this->getToken();
        }else{
            $token = $redisCache->get();
        }

        $apiUrl = $_ENV['PLEAD_API_URL'].$url;

        $headers = array();
        $headers['Content-Type'] = 'application/json';
        $headers['Authorization'] = " Bearer ".$token;
        $headers[] = "Authorization: Bearer ".$token;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiUrl);

        if($isPost){
            curl_setopt($ch, CURLOPT_POST, $isPost );
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        }else if($isDelete){
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'DELETE' );
        }else{
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );
        }


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);

        $result_curl = curl_exec( $ch );

        if ( curl_errno( $ch ) ){
            dd("curl_errno", curl_errno( $ch ));
            return false;
        }

        curl_close($ch);

        if(!$result = json_decode($result_curl)){
            $result_arr = explode("\n", $result_curl);

            if(!$result = json_decode(end($result_arr))){
                return $result_arr;
               // return false;
            }

        }
        if(isset($result->error) && $result->error == "invalid_token"){
            $this->getToken();
            return $this->request($url, $isPost, $data, $isDelete);
        }
        return $result;
    }

    

}
